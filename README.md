# ACE - Calculadora de Horas

[![Build status](https://ci.appveyor.com/api/projects/status/ve3n804gt6rrrfgm/branch/master?svg=true)](https://ci.appveyor.com/project/wkoch/ace/branch/master)
[![Build Status](https://travis-ci.com/wkoch/ace.svg?branch=master)](https://travis-ci.com/wkoch/ace)
![Dependencies Status](https://david-dm.org/wkoch/ace.svg)
[![devDependency Status](https://david-dm.org/wkoch/ace/dev-status.png)](https://david-dm.org/wkoch/ace#info=devDependencies)

Aplicativo calculador de horários.
